<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Acl
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Plugin\Acl\Exception;

//
use Throwable;

/**
 *
 */
interface ExceptionInterface extends Throwable {

}
